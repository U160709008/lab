package yemre.shapes;

public class Circle {
	int radius;
	
	public Circle (int radius) {
		this.radius = radius;
	}
	public double area() {
		return radius * radius * Math.PI;
	}
	
	public double perimeter() {
		return radius * 2 * Math.PI;
	}

}
