package yemre.main;
import yemre.shapes.Circle;
import java.util.ArrayList;
import yemre.shapes.Square;
public class Main {

	public static void main(String[] args) {
		ArrayList <Circle> circles = new ArrayList <Circle> ();
		
		circles.add(new Circle(5));
		circles.add(new Circle(3));
		circles.add(new Circle(4));
		circles.add(new Circle(10));
		
		for (int i = 0; i < circles.size(); i++) {
			System.out.println(circles.get(i).area());
			
		}
		
	}

}
