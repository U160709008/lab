
public class GCDLoop {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		//int a = 90 ;
		//int b = 50 ;
		if (a != 0 && b != 0) {
			int q ,r = 1;
			while (true) {
				r = a % b;
				q = a / b;
				a = (q*b) + r;
				a = b;
				if (r != 0 ) {
					b = r;
				}
					
				if (a == b && r == 0) {
					break;								
				}
			}
			System.out.println("The GCD is " + b);
		}else {
			System.out.println("Zero don't have GCD");
		}
		

	}

}
