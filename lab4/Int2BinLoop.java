import java.util.ArrayList;
public class Int2BinLoop {

	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		int remainder;
		int quotient;
		
		ArrayList<String> list = new ArrayList<String>();
		
		//int[] values = new int[number];
		while ( 0 < number ) {
			remainder = number % 2;
			quotient = number / 2;
			number = quotient;
			
			if (remainder == 1) {
				//System.out.print("1");
				list.add("1");
			}else {
				//System.out.print("0");
				list.add("0");
			}
		}
		//System.out.println(list);
		//int i = list.size() ;
		for (int i = list.size()-1 ; i >= 0; i--) {
			System.out.print(list.get(i));
		}
	}

}
