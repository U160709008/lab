package yemre.shapes3d;

import yemre.shapes.Circle;

public class Cylinder extends Circle {
	
	int height;
	
	public Cylinder() {
		this (5,10);
	}
	
	public Cylinder (int radius) {
		this(radius , 10);
	}
	
	public Cylinder (int radius , int height) {
		super(radius);
		this.height = height;
	}
	
	public double area() {
		return (super.perimeter() * height) + (super.area() * 2);	
	}
	
	public double volume() {
		return super.area() * height;
	}
	
	public String toString() {
		return "radius = " + radius+ " " +
				"height = " + height;
		
	}
}
