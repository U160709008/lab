package yemre.shapes;

public class Square {
	int side;
	
	public Square (int side) {
		this.side = side;
	}
	protected int area() {
		return side *side;
	}
	
	public int perimeter() {
		return 4 * side;
	}

}
