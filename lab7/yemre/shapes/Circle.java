package yemre.shapes;
public class Circle {
	protected int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}

	public double area() {
		double area = (radius * radius) * 3.14;
		return area;
	}
	
	public double perimeter() {
		double perimeter = 2 * 3.14 * radius;
		return perimeter;
	}


}
