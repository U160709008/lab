package lab;

public class Question2 {

	public static void main(String[] args) {
		int num1 = Integer.parseInt(args[0]);
		int num2 = Integer.parseInt(args[1]);
		System.out.println(min(num1 , num2));

	}
	
	private static int min (int a, int b) {
		return (a < b) ? a:b ;
	}

}
