package Question12;

import Question11.Rectangle;

public class Box extends Rectangle {
	int heigth;

	public Box(int length, int width, int heigth) {
		super(length, width);
		this.heigth = heigth;
	}
	
	public int area() {
		return super.area() * 6;
	}
	
	public int volume() {
		return super.area() * heigth;
	}

}
