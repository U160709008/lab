package stack;

import java.util.List;

public  class StackImpl<T> implements Stack<T> {
	
	StackItem<T> top;
	
	@Override
	public void push(T item) {
		StackItem<T> newTop = new StackItem<>(item, top);
		top = newTop;	
	}

	@Override
	public T pop() {
		T item = top.getItem();
		top = top.getPrevious();
		return item;
	}

	@Override
	public boolean empty() {
		return false;
	}

	@Override
	public List<T> toList() {
		// TODO Auto-generated method stub
		return null;
	}

}
